import json
from timeit import default_timer as timer
from typing import Any
from uuid import uuid4

import boto3

sns = boto3.client("sns")
sqs = boto3.resource("sqs")


class Message:
    def __init__(self, body: Any) -> None:
        self.body = body

    def send(self, topic: str) -> None:
        sns.publish(
            TargetArn="arn:aws:sns:eu-west-1:002025366337:" + topic,
            Message=json.dumps({"default": json.dumps(self.body)}),
            MessageStructure="json",
        )

    def send_and_get_response(
        self, request_topic: str, response_topic: str, timeout: int = 10000
    ) -> Any:
        # Set up SQS queue to receive reply and sub it to the SNS topic
        queue_name = response_topic + "-" + uuid4().hex[:5]
        policy = """{
          "Version": "2012-10-17",
          "Id": "arn:aws:sqs:eu-west-1:002025366337:%s/SQSDefaultPolicy",
          "Statement": [
            {
              "Sid": "Sid1553088901835",
              "Effect": "Allow",
              "Principal": "*",
              "Action": "SQS:SendMessage",
              "Resource": "arn:aws:sqs:eu-west-1:002025366337:%s"
            }
          ]
        }""" % (
            queue_name,
            queue_name,
        )
        queue = sqs.create_queue(QueueName=queue_name, Attributes={"Policy": policy})
        sns.subscribe(
            TopicArn="arn:aws:sns:eu-west-1:002025366337:" + response_topic,
            Protocol="sqs",
            Endpoint=queue.attributes.get("QueueArn"),
        )
        # Send request message
        self.send(request_topic)
        # Poll queue for response
        response = None
        start = timer()
        while (timer() - start) * 1000 < timeout:
            messages = queue.receive_messages()
            if messages:
                # Grab the response as a message
                response = json.loads(json.loads(messages[0].body)["Message"])
                # Break out of the while loop
                break
        # Destroy queue
        queue.delete()
        # Return response
        return response

    @classmethod
    def from_sns_event(cls, event):
        pass
