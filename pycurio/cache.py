import lzma
import pickle
from abc import ABC, abstractmethod
from typing import Any

import botocore

import boto3

s3 = boto3.client("s3")


class Cache(ABC):
    def __init__(self, use_compression: bool = True):
        self.use_compression = use_compression

    @abstractmethod
    def get(self, key: str) -> Any:
        pass

    @abstractmethod
    def set(self, key: str, obj: Any) -> None:
        pass

    @abstractmethod
    def delete(self, key: str) -> None:
        pass

    def serialize(self, obj) -> bytearray:
        obj = pickle.dumps(obj, 4)
        if self.use_compression:
            obj = lzma.compress(obj)
        return obj

    def deserialize(self, obj) -> Any:
        if self.use_compression:
            obj = lzma.decompress(obj)
        return pickle.loads(obj)


class S3Cache(Cache):
    def __init__(self, cache_bucket: str, use_compression: bool = True) -> None:
        self.cache_bucket = cache_bucket
        super().__init__(use_compression)

    def get(self, key: str) -> Any:
        try:
            return self.deserialize(
                s3.get_object(Bucket=self.cache_bucket, Key=key)["Body"].read()
            )
        except botocore.exceptions.ClientError:
            return None
    
    def get_with_meta(self, key: str) -> Any:
        try:
            obj = s3.get_object(Bucket=self.cache_bucket, Key=key)
            return self.deserialize(obj['Body'].read()), obj
        except botocore.exceptions.ClientError:
            return None, None

    def set(self, key: str, obj: Any) -> None:
        s3.put_object(Bucket=self.cache_bucket, Key=key, Body=self.serialize(obj))

    def delete(self, key) -> None:
        try:
            s3.delete_object(Bucket=self.cache_bucket, Key=key)
        except botocore.exceptions.ClientError:
            # Already deleted?
            return None
