import os

import jwt
import requests
from chalice import AuthResponse
from cryptography import x509
from cryptography.hazmat.backends import default_backend
from jwt.exceptions import InvalidTokenError

import sentry_sdk

class GoogleKeyManager:
    """Manager for Google's public keyset.

    Caches the keyset (until we get an unkown key ID), so that we don't have to
    re-fetch it for every single request.
    """

    def __init__(self):
        self.keys = None

    def get_key(self, kid):
        """Get the Google key with the given key ID."""
        if self.keys is None or kid not in self.keys:
            # We don't have the key list, or the key is not in there
            print(f"Fetching JWT keyset from Google for kid={kid}")
            r = requests.get(
                "https://www.googleapis.com/robot/v1/metadata/x509/securetoken@system.gserviceaccount.com"
            )
            self.keys = r.json()
        if kid in self.keys:
            return self.keys[kid]
        return None


gkm = GoogleKeyManager()


def get_auth_response(auth_request):
    try:
        token = auth_request.token[7:]
        header = jwt.get_unverified_header(token)
        if "kid" in header:
            # Looks like a Firebase-generated JWT
            # Check with Google's public key
            cert = x509.load_pem_x509_certificate(
                gkm.get_key(header["kid"]).encode("ascii"), backend=default_backend()
            )
            decoded = jwt.decode(
                token, cert.public_key(), algorithms=["RS256"], audience=["curio-cb6af", "curio-beta"]
            )
            sub = decoded["sub"]
            try:
                is_admin = decoded["staff"]
            except:
                is_admin = False
        else:
            # Looks like a monolith-generated JWT
            # Check with our own secret
            decoded = jwt.decode(token, os.environ["JWT_SECRET"], algorithms=["HS256"])
            sub = decoded["uuid"]
            try:
                is_admin = decoded["type"]["type"] == "developer"
            except:
                is_admin = False
                
        # set admin privileges
        context = {"is_admin": is_admin}
        return AuthResponse(routes=["*"], principal_id=sub, context=context)
    except KeyError as e:
        print(f"Auth failed - missing key {str(e)}")
        return AuthResponse(routes=[], principal_id=None)
    except InvalidTokenError as e:
        # Bad token
        print(f"Auth failed due to bad token - {str(e)}")
        return AuthResponse(routes=[], principal_id=None)
    except AttributeError as e:
        print(f"Auth failed due to headr key not found - {str(e)}")
        return AuthResponse(routes=[], principal_id=None)


def get_authorized_user_id(current_request):
    """Get the ID of the currently authenticated user.

    Args:
        current_request: The current request context.

    Returns:
        The user (UU)ID.

    """
    return current_request.context["authorizer"]["principalId"]


def is_admin(current_request):
    """Check if the currently authenticated user is an admin.

    Args:
        current_request: The current request context.

    Returns:
        True - if user has a developer account on the monolith, or
             - if user has a staff account on firebase
        False - regular boring people, i.e. not us

    """
    return current_request.context["authorizer"]["is_admin"]
