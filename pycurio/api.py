import json
from uuid import uuid4

from chalice import Response

import boto3


def make_response(
    body, headers=None, status_code=200, envelope=True, force_serve_from_bucket=False
) -> Response:
    if envelope:
        body = {"data": body}
    if force_serve_from_bucket or False:
        # Put it on S3 because it's so big
        filename = str(uuid4()) + ".json"
        s3 = boto3.resource("s3")
        bucket = s3.Bucket("big.curio.io")
        bucket.put_object(
            Key=filename,
            Body=json.dumps(body),
            # ACL='authenticated-read',
            ContentType="application/json",
        )
        url = "https://big.curio.io/" + filename
        headers["Location"] = url
        response = Response(status_code=303, body="", headers=headers)
    else:
        response = Response(body, headers=headers, status_code=status_code)
    return response
