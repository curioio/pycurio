import json 
import sentry_sdk

def func(event, get_response):
    try: 
        request = event.to_dict() 
        response = get_response(event)
        
        try: 
            body = event.json_body 
            request['body'] = body 
        except:
            pass
        
        log_message = json.dumps({ "request": request, "response": response.to_dict() })
        print(log_message)
    
        try: 
            path = request["path"]
            sentry_sdk.set_tag("path", path)
        except:
            pass 
        
        try: 
            platform = request["headers"]["platform"]
            sentry_sdk.set_tag("platform", platform)
        except: 
            pass 
        
        try: 
            appversion = request["headers"]["appversion"]
            sentry_sdk.set_tag("appversion", appversion)
        except: 
            pass 
        
        try: 
            user_id = request["context"]["authorizer"]["principalId"]
            sentry_sdk.set_user({"id": user_id})
        except: 
            pass
        
        try: 
            request_id = request["context"]["requestId"] 
            sentry_sdk.set_tag("request_id", request_id)
        except: 
            pass 
            
        try: 
            country = request["headers"]["cloudfront-viewer-country"]
            sentry_sdk.set_tag("country", country)
        except:
            pass
    
        return response
    
    except Exception as e:
        sentry_sdk.capture_exception(e)
        return response
