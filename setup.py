import setuptools

with open("README.md", "r") as f:
    long_description = f.read()

setuptools.setup(
    name="pycurio",
    version="0.0.5",
    author="Various @ Curio Labs Ltd.",
    description="Shared library for Python-based microservices at Curio",
    long_description=long_description,
    long_description_content_type="text/markdown",
    packages=setuptools.find_packages(),
    install_requires=["requests", "cryptography"],
    classifiers=(
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ),
)
