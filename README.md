
# pycurio

A shared library for common functions (auth, caching, etc).

## Installing / upgrading

Either way, run:

    pip install --upgrade -e git+https://bitbucket.org/curioio/pycurio.git#egg=pycurio

which will install the latest commit from this repo.